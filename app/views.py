# same as routes.py
# views.py is the file that contains the routes for our app
# We will create a blueprint in this file
# Blueprints are a way to organize related routes in a Flask app
# Blueprints are created in a separate file from the main app
# Blueprints are registered in the main app
import json
import os

from flask import Blueprint, render_template, request, jsonify

from .services.get_data import get_all_data, get_data_page, get_chart_data, get_grouped_products,get_grouped_products_for_simple_charts

views = Blueprint('views', __name__)


@views.route('/')
def home():
    return render_template('index.html')


@views.route('/data-table/')
def data_table():
    # Get query parameters with default values
    page = int(request.args.get('page', 1))  # Default to the first page
    items_per_page = int(request.args.get('page_size', 10))  # Default to 10 items per page
    sort_by = request.args.get('sort', None)  # Default is handled in get_data_page
    sort_order = request.args.get('order', 'asc')  # Default to ascending order

    # Specify the CSV file path here
    csv_file = os.path.join('app', 'data', '1000_fake_products.csv')

    # Get the data for the requested page using the get_data_page function
    try:
        data = get_data_page(csv_file, page, items_per_page, sort_by, sort_order)

        # Render the template with the data
        return render_template('data_table.html', **data)
    except FileNotFoundError:
        return "CSV file not found", 404
    except Exception as e:
        return str(e), 500


@views.route('/charts')
def charts():
    try:
        # Using the new function to get aggregated data as a JSON string
        json_data = get_grouped_products_for_simple_charts()

        # Converting JSON string back to Python dictionary
        data = json.loads(json_data)

        # Check if chart headers exist in the data
        if not data['chart_headers']:
            raise Exception('No headers found in data')

        # Dictionary to hold chart data
        charts_data = {}

        # Prepare data for Chart.js
        for header in data['chart_headers']:
            if header != 'category':  # Skipping 'category' header for chart data
                # Extracting values for the current header
                values = [entry[header] for entry in data['chart_data']]

                # Labels are the categories
                labels = [entry['category'] for entry in data['chart_data']]

                # Storing the labels and values for each header
                charts_data[header] = {
                    'labels': labels,
                    'values': values,
                }

        # Return the chart data as a JSON response
        return render_template('charts.html', charts_data=charts_data)


    except FileNotFoundError:
        return jsonify({'error': 'CSV file not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@views.route('/old-charts')
def old_charts():
    try:
        data = get_all_data()

        if not data['headers']:
            raise Exception('No headers found in data')

        # Dictionary to hold chart data for each header
        charts_data = {}

        # Prepare data for Chart.js for each header
        for header in data['headers']:
            # Initialize the value counts for each header
            value_counts = {}

            # Count the occurrences of each value for the header
            for entry in data['data']:
                value = entry[header]
                value_counts[value] = value_counts.get(value, 0) + 1

            # Store the labels and sizes for each header
            charts_data[header] = {
                'labels': list(value_counts.keys()),
                'sizes': list(value_counts.values()),
            }

    except FileNotFoundError:
        return render_template('404.html'), 404
    except Exception as e:
        return render_template('server_error.html', error=str(e)), 500

    # Return all headers and their chart data to the template
    return render_template('old_charts.html', headers=data['headers'], charts_data=charts_data)


@views.route('/detailed-charts')
def detailed_charts():
    try:
        # Using the new function to get aggregated data as a JSON string
        json_data = get_grouped_products()

        # Converting JSON string back to Python dictionary
        data = json.loads(json_data)

        # Check if chart headers exist in the data
        if not data['chart_headers']:
            raise Exception('No headers found in data')

        # Dictionary to hold chart data
        charts_data = {}

        # Prepare data for Chart.js
        for header in data['chart_headers']:
            if header != 'category':  # Skipping 'category' header for chart data
                # Extracting values for the current header
                values = [entry[header] for entry in data['chart_data']]

                # Labels are the categories
                labels = [entry['category'] for entry in data['chart_data']]

                # Storing the labels and values for each header
                charts_data[header] = {
                    'labels': labels,
                    'values': values,
                }

        # Return the chart data as a JSON response
        return render_template('detailed_charts.html', charts_data=charts_data)


    except FileNotFoundError:
        return jsonify({'error': 'CSV file not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 500
