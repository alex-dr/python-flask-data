from flask import Flask, render_template

def create_app():
    app = Flask(__name__)

    # import the routes
    from .views import views

    # register the routes
    app.register_blueprint(views, url_prefix='/')

    @app.errorhandler(404)
    def page_not_found(e):
        # note that we set the 404 status explicitly
        return render_template('not_found.html'), 404

    @app.errorhandler(500)
    def internal_server_error(e):
        # note that we set the 404 status explicitly
        return render_template('server_error.html'), 500

    return app
