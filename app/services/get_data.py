# services/get_data.py

import json
import os

import pandas as pd


def get_data_page(csv_file, page=1, items_per_page=10, sort_by=None, sort_order='asc'):
    try:
        df = pd.read_csv(csv_file)
        if not sort_by:
            sort_by = df.columns[0]  # Default sort by the first column
        if not sort_order:
            sort_order = 'asc'  # Default sort order

        df = df.sort_values(by=sort_by, ascending=sort_order == 'asc')
        total_pages = (len(df) + items_per_page - 1) // items_per_page
        start = (page - 1) * items_per_page
        end = start + items_per_page
        current_page_data = df.iloc[start:end]
        data = current_page_data.to_dict(orient='records')
        headers = df.columns.tolist()

        return {
            'data': data,
            'total_pages': total_pages,
            'current_page': page,
            'sort_by': sort_by,
            'sort_order': sort_order,
            'headers': headers,
            'items_per_page': items_per_page
        }

    except FileNotFoundError:
        raise FileNotFoundError('CSV file not found')
    except Exception as e:
        raise Exception('Internal Server Error')


def get_all_data():
    csv_file = os.path.join('app', 'data', '1000_employees.csv')
    csv_file = os.path.join('app', 'data', '1000_fake_products.csv')

    try:
        df = pd.read_csv(csv_file)
        data = df.to_dict(orient='records')
        headers = df.columns.tolist()

        return {
            'data': data,
            'headers': headers
        }

    except FileNotFoundError:
        raise FileNotFoundError('CSV file not found')
    except Exception as e:
        raise Exception('Internal Server Error')


def get_grouped_products_for_simple_charts():
    csv_file = os.path.join('app', 'data', '1000_fake_products.csv')

    try:
        # DataFrame
        df = pd.read_csv(csv_file)

        # Grouping data by 'category' and calculating relevant statistics
        grouped_data = df.groupby('category').agg(
            total_value=pd.NamedAgg(column='price', aggfunc='sum'),
            min_price=pd.NamedAgg(column='price', aggfunc='min'),
            max_price=pd.NamedAgg(column='price', aggfunc='max'),
            total_net_profit=pd.NamedAgg(column='net profit', aggfunc='sum'),
            average_net_profit_percent=pd.NamedAgg(column='net profit %', aggfunc='mean'),
            total_units=pd.NamedAgg(column='id', aggfunc='count')  # Counting total products in each category

        ).reset_index()

        # Converting the aggregated data to a dictionary
        chart_data = grouped_data.to_dict(orient='records')

        # Preparing headers for the chart data
        chart_headers = grouped_data.columns.tolist()

        # Prepare data for JSON format
        data_for_json = {
            'chart_data': chart_data,
            'chart_headers': chart_headers
        }

        # Convert dictionary to JSON string
        json_string = json.dumps(data_for_json)

        return json_string

    except FileNotFoundError:
        raise FileNotFoundError('CSV file not found')
    except Exception as e:
        raise Exception('Internal Server Error')


def get_grouped_products():
    csv_file = os.path.join('app', 'data', '1000_fake_products.csv')

    try:
        df = pd.read_csv(csv_file)

        # Grouping data by 'category' and calculating relevant statistics
        grouped_data = df.groupby('category').agg(
            total_value=pd.NamedAgg(column='price', aggfunc='sum'),
            total_net_profit=pd.NamedAgg(column='net profit', aggfunc='sum'),
            average_net_profit_percent=pd.NamedAgg(column='net profit %', aggfunc='mean')
        ).reset_index()

        # Converting the aggregated data to a dictionary
        chart_data = grouped_data.to_dict(orient='records')

        # Preparing headers for the chart data
        chart_headers = grouped_data.columns.tolist()

        # Prepare data for JSON format
        data_for_json = {
            'chart_data': chart_data,
            'chart_headers': chart_headers
        }

        # Convert dictionary to JSON string
        json_string = json.dumps(data_for_json)

        return json_string

    except FileNotFoundError:
        raise FileNotFoundError('CSV file not found')
    except Exception as e:
        raise Exception('Internal Server Error')


def get_all_data_for_detailed_charts():
    csv_file = os.path.join('app', 'data', '1000_employees.csv')
    csv_file = os.path.join('app', 'data', '1000_products.csv')

    try:
        df = pd.read_csv(csv_file)
        data = df.to_dict(orient='records')
        headers = df.columns.tolist()

        return {
            'data': data,
            'headers': headers
        }

    except FileNotFoundError:
        raise FileNotFoundError('CSV file not found')
    except Exception as e:
        raise Exception('Internal Server Error')


def get_chart_data(csv_file):
    try:
        df = pd.read_csv(csv_file)
        # Ensure 'Category' is a column in your CSV
        df = df[['Category', 'Price range', 'Net profit range']]

        # Convert 'Price range' and 'Net profit range' from a range format to a numeric midpoint
        df['Price range'] = df['Price range'].apply(parse_range)
        df['Net profit range'] = df['Net profit range'].apply(parse_range)

        # Group the data by 'Category' and calculate the mean for the numeric columns
        chart_data = df.groupby('Category').agg({
            'Price range': 'mean',
            'Net profit range': 'mean'
        }).to_dict(orient='index')

        # Round the mean values to two decimal places
        for category, data in chart_data.items():
            data['Price range'] = round(data['Price range'], 2)
            data['Net profit range'] = round(data['Net profit range'], 2)

        # Get the list of headers from the DataFrame
        headers = list(df.columns)

        return {'headers': headers, 'data': chart_data}

    except FileNotFoundError:
        raise FileNotFoundError('CSV file not found')
    except Exception as e:
        raise Exception(f'Internal Server Error: {e}')


def parse_range(value):
    # Remove the dollar sign and split the string into the low and high end of the range
    low, high = value.replace('$', '').split('-')
    # Convert the low and high values to floats and return the midpoint
    return (float(low) + float(high)) / 2
