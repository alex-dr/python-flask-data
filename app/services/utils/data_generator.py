import csv
import random

# Define the header and possible values for categories
header = ['id', 'name', 'category', 'price', 'net profit', 'net profit %']
categories = ['Electronics', 'Clothing', 'Food', 'Home Decor', 'Toys', 'Sports', 'Books', 'Beauty', 'Furniture', 'Jewelry']

# Define the price ranges for each category
price_ranges = {
    'Electronics': (150, 18000),
    'Clothing': (15, 900),
    'Food': (1, 400),
    'Home Decor': (20, 3000),
    'Toys': (15, 800),
    'Sports': (50, 1900),
    'Books': (10, 130),
    'Beauty': (20, 700),
    'Furniture': (60, 4000),
    'Jewelry': (200, 12000)
}

# Generate 1000 sample products with numeric IDs
products = []
for product_id in range(1, 1001):  # IDs from 1 to 1000
    name = f'Product {product_id}'
    category = random.choice(categories)
    price_range = price_ranges[category]
    price = round(random.uniform(*price_range), 2)  # Use the specific price range for the category
    max_net_profit = min(price * 0.5, round(random.uniform(2, price * 0.5), 2))  # Ensure net profit is <= 50% of price
    net_profit_percentage = round((max_net_profit / price) * 100, 2) if price != 0 else 0  # Calculate net profit percentage
    products.append([product_id, name, category, price, max_net_profit, net_profit_percentage])

# Save the data to a CSV file
csv_file_path = './../../data/1000_fake_products.csv'

with open(csv_file_path, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(header)
    writer.writerows(products)

print(f"{csv_file_path} has been created with numeric IDs, realistic prices, and net profit percentages, limited to 50% of price.")
